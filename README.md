django-autopages
================

This application scans specified directories with Django-templates and
adds URL for every template it finds using Django's view `direct_to_templates`.

Installation
------------

1. Install package from repository:

        pip install -e https://bitbucket.org/zcho05/django-autopages.git#egg=autopages

2. Add `'autopages'` to your `settings.INSTALLED_APPS`.
3. Add following lines to your `urls.py`:

        from autopages.urls import autopages
        urlpatterns += autopages('autopages')

4. Now you can add templates to `templates/autopages/` and __after restart of server__ you'll
be able to see them. E.g. for template `templates/autopages/about.html` corresponding URL is
`/about/`, and for `templates/autopages/more/faq.html` URL is `/more/faq/`.

License
-------

Copyright © 2013, onzuka

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.